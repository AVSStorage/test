<?php

if (isset($_POST['order_id'])) {
    $orderId = $_POST['order_id'];
    if ($order = DB::query("SELECT o1.`order_id`, o2.`order_status`
        FROM `orders` o1
                 INNER JOIN `order_status` o2 ON o2.`order_id` = o1.`order_id`
        WHERE o1.`order_id` = {$orderId}")) {
        
         $successOrderMessage = [
                'status'       => 'success',
                'order_id'     => (int)$order['order_id'],
                'order_status' => $order['order_status'],
            ];
           try {
                sendJson (
                    $successOrderMessage
                );
            } catch (TypeError $e) {
                print($e);
            }
           
        } else {
        
            $failedOrderMessage =  [
                    'status'  => 'fail',
                    'message' => sprintf('Заказ с ID `%x` не найден', $orderId)
                ];
            try {
                    sendJson (
                        $failedOrderMessage
                    );
                }
            catch (TypeError $e) {
                print($e);
            }

            
    }
}

/**
     * Show json encoded order message.
     *
     * @param array $array Fields of order message.

*/

function sendJson(array $array) : void
{
    print(json_encode($array));
}

?>
